import * as React from 'react';
import { NavMenu } from './NavMenu';

export interface LayoutProps {
    children?: React.ReactNode;
}

export class Layout extends React.Component<LayoutProps, {}> {
    public render() {
        return <div className='container-fluid'>

            <div className='row'>
                <NavMenu />
            </div>

            <div id="masthead">
                <div className="row">
                    <div className="small-12 columns">
                       
                        <div id="logotext">
                            <div id="logoheadline">Bike2Work Challenge</div>
                            <div id="logosubheadline">A CERN mobility challenge</div>
                        </div>
                       
		            </div>
	            </div>
            </div>
            <div className='row'>
                <div className='col-sm-2'></div>
                <div className='col-sm-8'>
                    { this.props.children }
                </div>
                <div className='col-sm-2'></div>
            </div>
        </div>;
    }
}
