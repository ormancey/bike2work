import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Redirect } from 'react-router-dom';
import * as Modal from 'react-modal';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};
// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#react-app')

interface SigninState {
    login: string;
    authenticated: boolean;
    modalIsOpen: boolean;
}

export class Signin extends React.Component<any, SigninState> {
    constructor(props: any) {
        super(props);
        this.state = { login: '', authenticated: false, modalIsOpen: false };

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);

        //this.getUserInfoASync();
        this.signedok = this.signedok.bind(this);
    }

    componentDidMount() {
        console.log('componentDidMount.');
        this.getUserInfo();
    }

    signedok() {
        this.props.signinhandler();
    }

    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({ modalIsOpen: false });
    }

    public render() {
        let loginlink = (
            <div>
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel="Login with CERN SSO"
                >
                   
                    <h2>Login with CERN SSO</h2>
                    <button onClick={ this.login.bind(this) }>Click to Sign in</button>
                </Modal>
            </div>
            );

        let contents = this.state.authenticated
            ? "Authenticated as " + this.state.login
            : loginlink;
            //: <button onClick={() => { this.login() }}>Login with CERN SSO</button>; //<Redirect to='/login' />;

        return <div>
            {contents}
        </div>;
    }

    login() {
        var login_window = window.open('/api/Commutes/Authenticate/', 'login', 'toolbar=0,status=0,width=800,height=600');
        // Wait for popup to close then refresh
        var component = this;
        var interval = window.setInterval(( function() {
            if (login_window == null)
                return;
            if (login_window.closed) {
                window.clearInterval(interval);
                component.closeModal();
                component.getUserInfo();
            }
        }), 1000);
    }
    
    getUserInfo() {
        var component = this;
        fetch('api/Commutes/GetUserInfo/', { method: 'GET', redirect: 'error', credentials: "same-origin" })
            .then(response => response.json() as Promise<string>)
            .then(data => {
                this.setState({ login: data, authenticated: true, modalIsOpen: false });
                this.signedok();
            })
            .catch(function (err) {
                console.info('getUserInfo error, user not authenticated: ' + err);
                component.openModal();
            });
    }

    //async getUserInfoASync() {
    //    try {
    //        let response = await fetch('api/Commutes/GetUserInfo/', { method: 'GET', redirect: 'error', credentials: "same-origin" });
    //        let data = await response.json() as string;
    //        console.log('getUserInfoASync returned: ' + data);
    //        //this.setState({ login: data, authenticated: true });
    //        this.state = { login: data, authenticated: true, modalIsOpen: false };
    //        //return true;
    //    } catch (error) {
    //        console.log(error);
    //        //return false;
    //    }
    //}

    //public isAuthenticated(): boolean {
    //    var component = this;
    //    var isuserauthenticated = new Promise<SigninState>(function (resolve, reject) {
    //        var res = Signin.getUserInfo();
    //        console.log('isAuthenticated promise returning: ' + res.authenticated + ' ' + res.login);
    //        resolve(res);
    //    });

    //    isuserauthenticated.then(function (fulfilled: SigninState) {
    //        console.log('isAuthenticated returning: ' + fulfilled.authenticated + ' ' + fulfilled.login);
    //        return fulfilled.authenticated;
    //    });

    //    return false;
    //}

}

