﻿

export const CommuteTypes = {
    off: "off",
    bike: "bike",
    walk: "walk",
    publictransport: "publictransport",
    electricdevice: "electricdevice",
    sharedcar: "sharedcar",
    car: "car",
    other: "other"
}
