import * as React from 'react';
import { RouteComponentProps } from 'react-router';

interface RankingState {
    currentCount: number;
}

export class Ranking extends React.Component<RouteComponentProps<{}>, RankingState> {
    constructor() {
        super();
        this.state = { currentCount: 0 };
    }

    public render() {
        return <div>
            <h1>Ranking</h1>

            <p>This is a simple example of a React component.</p>

        </div>;
    }

    incrementCounter() {
        this.setState({
            currentCount: this.state.currentCount + 1
        });
    }
}
