import * as React from 'react';
import { RouteComponentProps, Redirect } from 'react-router';
import Calendar from 'react-calendar';
import * as Moment from 'moment';
import { CommuteTypes } from './CommuteTypes';
import { Signin } from './Signin';

class Commute {
    id?: number;
    login: string;
    year?: number;
    month?: number;
    day?: number;
    distance: number;
    commutetype: string;
}

function getDate(onecommute: Commute): Date {
    return Moment(onecommute.day + '/' + onecommute.month + '/' + onecommute.year, 'DD/MM/YYYY').toDate();
}

function setDate(oneCommute: Commute, date: Date): Commute {
    var check = Moment(date);
    oneCommute.day = check.date();
    oneCommute.month = 1 + check.month();
    oneCommute.year = check.year();
    //alert('yes ' + value);
    return oneCommute;
}

interface MyCalendarState {
    currentCommute: Commute,
    loading: boolean,
    commutes: Commute[];
}

export class MyCalendar extends React.Component<RouteComponentProps<{}>, MyCalendarState> {
    constructor() {
        super();
        var newC = new Commute();
        newC.distance = 0;
        newC.commutetype = "off";
        newC.login = 'ormancey';

        this.state = {
            currentCommute: setDate( newC , new Date() ),
            loading: true,
            commutes: []
        };

        // Load data
        //this.loadCommutes();
        this.signinhandler = this.signinhandler.bind(this);
    }

    //componentDidMount() {
    //    console.log('componentDidMount.');
    //    // Load data
    //    this.loadCommutes();
    //}
    signinhandler() {
        //e.preventDefault()
        console.log('signinhandler triggered');
        // Load data
        this.loadCommutes();
    }

    handledistanceChange(e: any) {
        var tmp = this.state.currentCommute;
        tmp.distance = e.target.value;
        this.setState({ currentCommute: tmp });
    }
    handlecommutetypeChange(e: any) {
        var tmp = this.state.currentCommute;
        tmp.commutetype = e.target.value;
        this.setState({ currentCommute: tmp });
    }
    showDay(selectedDay: Date) {
        console.log("Selected day: " + selectedDay);

        var yearchanged = selectedDay.getFullYear() != this.state.currentCommute.year;

        // Load currentCommute data if it is already filled
        if (this.isfilled(selectedDay)) {
            var result = this.state.commutes.filter(e => Moment(selectedDay).isSame(getDate(e)))[0];

            var tmp = this.state.currentCommute;
            tmp = setDate(tmp, selectedDay);
            tmp.commutetype = result.commutetype;
            tmp.distance = result.distance;
            tmp.login = result.login;
            tmp.id = result.id;

            this.setState({ currentCommute: tmp });
        }
        else {
            var tmp = this.state.currentCommute;
            tmp.id = undefined;
            tmp = setDate(tmp, selectedDay);
            this.setState({ currentCommute: tmp });
        }

        console.log("new current Commute day: " + this.state.currentCommute.day + '/' + this.state.currentCommute.month + '/' + this.state.currentCommute.year);

        // Reload data if year changed
        if (yearchanged)
            this.loadCommutes();
    }

    public render() {

        return <div>
            <h1>My Commuting Calendar</h1>

            <Signin signinhandler={this.signinhandler} />

            <div className="row">
                <div className="col-sm-6">
                    <h4>Select your day</h4>
                    <Calendar
                        tileClassName={({ date, view }) => view === 'month' ? this.filledcssclass(date) : ''}
                        onClickDay={(value) => this.showDay(value)}
                        value={getDate(this.state.currentCommute)}
                        locale="en-GB"
                            />
                 </div>

                <div className="col-sm-6 inputcommute">
                    <h4>Input your Commute</h4>
                    <div className="header"><h3>{Moment(getDate(this.state.currentCommute)).format("DD/MM/YYYY")}</h3></div>
                    <form className="form-horizontal">
                        <div className="form-group">
                            <label className="col-sm-3">Distance:</label>
                            <div className="col-sm-9"><input type="text" className="form-control" name="distance" placeholder="distance" value={this.state.currentCommute.distance} onChange={this.handledistanceChange.bind(this)} /></div>
                        </div>
                        <div className="form-group">
                            <label className="col-sm-3">Type:</label>
                            <div className="col-sm-9">
                                <label className="radio-inline"><input type="radio" name="commutetype" value={CommuteTypes.off} onChange={this.handlecommutetypeChange.bind(this)} />{CommuteTypes.off}</label>
                                <label className="radio-inline"><input type="radio" name="commutetype" value={CommuteTypes.bike} onChange={this.handlecommutetypeChange.bind(this)} />{CommuteTypes.bike}</label>
                                <label className="radio-inline"><input type="radio" name="commutetype" value={CommuteTypes.other} onChange={this.handlecommutetypeChange.bind(this)} />{CommuteTypes.other}</label>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-offset-3 col-sm-9">
                                <button onClick={(e) => { this.saveCommute(e) }} className="btn btn-default">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div className="row">
                <h4>Review your Commutes for {this.state.currentCommute.year}</h4>
                <CommutesTable commutes={this.state.commutes} loading={this.state.loading} />
            </div>
        </div>;
    }

    loadCommutes() {
        this.setState({ loading: true });

        // Load commutes for currentyear
        console.log('GET commutes for ' + this.state.currentCommute.year);

        fetch('api/Commutes/GetYearlyCommutes/' + this.state.currentCommute.year, { credentials: "same-origin" })
            .then(response => response.json() as Promise<Commute[]>)
            .then(data => {
                this.setState({ commutes: data, loading: false });
            });
    }

    saveCommute(e: any) {
        e.preventDefault();

        console.log('Object to create or update:');
        console.log(JSON.stringify(this.state.currentCommute));

        var component = this;

        var promise = new Promise(function (resolve, reject) {

            // Updating existing entry
            if (component.state.currentCommute.id && component.state.currentCommute.id > 0) {
                console.log('PUT for update ' + component.state.currentCommute.id);
                fetch('api/Commutes/PutCommute/' + component.state.currentCommute.id, {
                    method: 'put',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    credentials: "same-origin",
                    body: JSON.stringify(component.state.currentCommute)
                })
                    .then(function (response) {
                        if (response.status !== 204)
                            console.log('PUT not ok: ' + response.status);
                        else {
                            console.log('PUT ok');
                            resolve(true);
                        }
                    })
                    .catch(function (err) {
                        console.log('error: ${err}');
                    });
            }
            // New entry
            else {
                console.log('POST for new entry');
                fetch('api/Commutes/PostCommute/', {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    credentials: "same-origin",
                    body: JSON.stringify(component.state.currentCommute)
                })
                    .then(function (response) {
                        if (response.status !== 201) {
                            console.log('POST not ok: ' + response.status);
                        } else {
                            //response.json().then(function (data) {
                            console.log('POST ok');
                            //});
                            resolve(true);
                        }
                    })
                    .catch(function (err) {
                        console.log('error: ${err}');
                    });
            }
        });

        // Reload data if all good after completion
        promise.then(bool => component.loadCommutes());

        // Reload data
        //this.loadCommutes();
    }

    isfilled(date: Date) {
        var result = this.state.commutes.some(e => Moment(date).isSame(getDate(e)));
        if (result)
            return true;
        return false;
    }

    filledcssclass(date: Date): string {
        var result = this.state.commutes.find(e => Moment(date).isSame(getDate(e)));
        if (result != undefined) {
            return result.commutetype + 'filled';
        }
        return '';
    }
}

class CommutesTable extends React.Component<any> {

    render() {
        let contents = this.props.loading
            ? <p><em>Loading...</em></p>
            : this.renderCommutesTable(this.props.commutes);

        return contents;
    }

    renderCommutesTable(commutes: Commute[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Distance (km)</th>
                    <th>Type</th>
                </tr>
            </thead>
            <tbody>
                {commutes.map(commute =>
                    <tr key={commute.id}>
                        <td>{commute.day}/{commute.month}/{commute.year}</td>
                        <td>{commute.distance}</td>
                        <td>{commute.commutetype.toString()}</td>
                    </tr>
                )}

                {
                    Object.keys(CommuteTypes).map(ctype => {
                            var tmp = commutes.filter(({ commutetype }) => commutetype === ctype).reduce((sum, i) => (sum += i.distance), 0);
                            if (tmp > 0) {
                                return <tr key={ctype} className={ctype + 'filled'}>
                                    <td className="text-right">Total {ctype}</td>
                                    <td>{tmp} km</td>
                                    <td></td>
                                </tr>
                            }
                        }
                    )
                }

                <tr>
                    <td className="text-right">Grand Total</td>
                    <td>{commutes.reduce((sum, i) => (sum += i.distance), 0)} km</td>
                    <td></td>
                </tr>

            </tbody>
        </table>;
    }
}
