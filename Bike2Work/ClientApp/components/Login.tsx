import * as React from 'react';
import { RouteComponentProps } from 'react-router';

export class Login extends React.Component<RouteComponentProps<{}>, {}> {

    
    public render() {
        
        return <div>
            <h1>CERN Commuting Challenge</h1>
            <p>Welcome to the CERN Commuting Challenge (replacing the Yearly Bike to CERN contest)</p>
            <div>
                <button onClick={() => { this.login() }}>Login with CERN SSO</button>
            </div>
        </div>;
    }

    login() {
        var login_window = window.open('/api/Commutes/Authenticate/', 'login', 'toolbar=0,status=0,width=800,height=600');
        // Wait for popup to close then refresh
        var component = this;
        var interval = window.setInterval((function () {
            if (login_window == null)
                return;
            if (login_window.closed) {
                window.clearInterval(interval);
                //component.getUserInfo();
                // Redirect where we came from
                
            }
        }), 1000);
    }

}
