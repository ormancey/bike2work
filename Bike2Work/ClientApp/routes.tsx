import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { MyCalendar } from './components/MyCalendar';
import { Ranking } from './components/Ranking';
import { Login } from './components/Login';

export const routes = <Layout>
    <Route exact path='/' component={ Home } />
    <Route path='/counter' component={Counter} />
    <Route path='/fetchdata' component={FetchData} />
    <Route path='/calendar' component={MyCalendar} />
    <Route path='/ranking' component={Ranking} />
    <Route path='/login' component={Login} />
</Layout>;
