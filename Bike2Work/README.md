﻿# react-calendar
cd project folder
then run 
`npm install react-calendar`

https://github.com/wojtekmaj/react-calendar

# entityframework 
https://docs.microsoft.com/en-us/ef/core/get-started/aspnetcore/new-db

### postgres
http://www.npgsql.org/efcore/index.html
Install-Package Npgsql.EntityFrameworkCore.PostgreSQL
Install-Package Microsoft.EntityFrameworkCore.Tools

### For local testing, in-memory 
https://docs.microsoft.com/en-us/ef/core/providers/in-memory/index
install-package Microsoft.EntityFrameworkCore.InMemory

instanciate dfor debug
services.AddDbContext<Bike2WorkContext>(options => options.UseInMemoryDatabase(databaseName: "bike2workDB"));

### Moments for Date formatting
https://momentjs.com/
npm install moment --save


### Authentication with OAuth

### Redirecto to login is not authentciated
https://medium.com/the-many/adding-login-and-authentication-sections-to-your-react-or-react-native-app-7767fd251bd1
use context
https://reactjs.org/docs/context.html
https://hackernoon.com/how-to-use-the-new-react-context-api-fce011e7d87

### Secrets manager for local debug
https://docs.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-2.0
https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-2.0&tabs=windows
nuget install-package command fails, just add it in the csproj file:
	`<DotNetCliToolReference Include="Microsoft.Extensions.SecretManager.Tools" Version="2.0.2" />`
cmd -> `dotnet user-secrets -h`
right click on project -> 'Manage user secrets'
Automatically loaded by WebHost.CreateDefaultBuilder(args) (in program.cs)
Just acces variables with _moviesApiKey = Configuration["MoviesApiKey"];

env or secrets:
https://blogs.msdn.microsoft.com/mihansen/2017/09/10/managing-secrets-in-net-core-2-0-apps/


# Required variables:
openid_redirecturi is needed if running in openshift, as internal is http and external is https we can't use the computed redirect_uri

# Openshift publishing
Following https://github.com/redhat-developer/s2i-dotnetcore/blob/master/2.0/build/README.md
at TOP LEVEL add .s2i/environment file containing 
DOTNET_STARTUP_PROJECT: Bike2Work/Bike2Work.csproj

Or use BUILD Environment of OpenShift (seems easier !)

# Fix fsevent npm errors
Recurrent error: `npm ERR! notsup Unsupported platform for fsevents@1.2.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})`
add to packages.json:
```
"optionalDependencies": {
   "fsevents": "*"
 }
 ```