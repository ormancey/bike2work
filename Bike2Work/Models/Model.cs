﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Bike2Work.Models
{
    //public enum ecommutetype { off, bike, walk, publictransport, electricdevice, sharedcar, car, other }

    public class Bike2WorkContext : DbContext
    {
        public Bike2WorkContext(DbContextOptions<Bike2WorkContext> options)
            : base(options)
        { }

        //public DbSet<Blog> Blogs { get; set; }
        public DbSet<DailyCommute> DailyCommutes { get; set; }
    }

    public class DailyCommute
    {
        [Key]
        public int id { get; set; }
        public string login { get; set; }
        public int year { get; set; }
        public int month { get; set; }
        public int day { get; set; }
        //public DateTime date { get; set; }
        public int distance { get; set; }
        public string commutetype {get; set; }
    }

}
