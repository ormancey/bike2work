using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Bike2Work.Models;
using System.Net.Http;

namespace Bike2Work
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureDevelopmentServices(IServiceCollection services)
        {
            services.AddDbContext<Bike2WorkContext>(options => options.UseInMemoryDatabase(databaseName: "bike2workDB"));

            FinalizeConfigureServices(services);
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureProductionServices(IServiceCollection services)
        {
            //var connectionString = Configuration.GetConnectionString("postgres_connectionstring");
            //services.AddDbContext<Bike2WorkContext>(options => options.UseNpgsql(connectionString));

            services.AddDbContext<Bike2WorkContext>(options => options.UseInMemoryDatabase(databaseName: "bike2workDB"));


            FinalizeConfigureServices(services);
        }

        public void FinalizeConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            // Database init is specified in Configure{envirronmentname}Services

            services.AddAuthentication(options =>
                            {
                                options.DefaultScheme = "Cookies";
                                options.DefaultChallengeScheme = "oidc";
                            })
                            .AddCookie("Cookies")
                            .AddOpenIdConnect("oidc", options =>
                            {   // Redirect uri  http://localhost:54078/signin-oidc
                                options.SignInScheme = "Cookies";
                                options.Authority = Configuration["openid_authority"]; // "https://keycloak-dev-01.cern.ch/auth/realms/master/protocol/openid-connect/auth";
                                options.MetadataAddress = Configuration["openid_metadataaddress"]; // "https://keycloak-dev-01.cern.ch/auth/realms/master/.well-known/openid-configuration";
                                options.RequireHttpsMetadata = true;
                                options.TokenValidationParameters.NameClaimType = "preferred_username";

                                options.ResponseType = Configuration["openid_responsetype"];
                                options.ClientId = Configuration["openid_clientid"]; // "bike_to_work";
                                options.ClientSecret = Configuration["openid_clientsecret"]; // 
                                options.SaveTokens = true;

                                // Dangerous, but current Openshift s2i image fails to allow custom root CA trusts
                                options.BackchannelHttpHandler = new HttpClientHandler() { ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator };

                                // Must specify this here because openshift runs a http server while the exposed site is https
                                options.Events.OnRedirectToIdentityProvider = async n =>
                                {
                                    n.ProtocolMessage.RedirectUri = Configuration["openid_redirecturi"];
                                    await Task.FromResult(0);
                                };
                });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true,
                    ReactHotModuleReplacement = true
                });

                
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
