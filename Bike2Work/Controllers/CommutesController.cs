﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Bike2Work.Models;
using Microsoft.AspNetCore.Authorization;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;
using System.Net;

namespace Bike2Work.Controllers
{
    [Produces("application/json")]
    [Route("api/Commutes/[action]")]
    public class CommutesController : Controller
    {
        private readonly Bike2WorkContext _context;

        public CommutesController(Bike2WorkContext context)
        {
            _context = context;
        }

        // GET: api/Commutes
        //[HttpGet]
        //public IEnumerable<DailyCommute> GetDailyCommutes()
        //{
        //    return _context.DailyCommutes;
        //}

        [Authorize]
        [HttpGet]
        public string GetUserInfo()
        {
            //string ret = "User Info: ";
            return HttpContext.User.Identity.Name;
            //ret += username;
            //            return ret;
        }

        [Authorize]
        [HttpGet]
        public ContentResult Authenticate()
        {
            string ret = "<html><head>\r\n";

            ret += "<script language=\"javascript\" type=\"text/javascript\">\r\n";
            ret += "function windowClose() {\r\n";
            ret += "window.open('', '_parent', '');\r\n";
            ret += "window.close();\r\n";
            ret += "}\r\n";
            ret += "</script>\r\n";

            ret += "<body onload='windowClose();'>";
            ret += "<input type=\"button\" value=\"Close this window\" onclick=\"windowClose();\">";
            ret += "</body></html>"; 

            return new ContentResult
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
                Content = ret
            };
        }


        [Authorize]
        [HttpGet("{year}")]
        public async Task<IActionResult> GetYearlyCommutes([FromRoute] int year)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var dailyCommutes =  await _context.DailyCommutes.ToListAsync(m => (m.year == year) && (m.month == month) );
            var dailyCommutes = _context.DailyCommutes.Where(m => m.year == year);
            dailyCommutes = dailyCommutes.OrderByDescending(s => s.year).ThenByDescending(s => s.month).ThenByDescending(s => s.day);

            if (dailyCommutes == null)
            {
                return NotFound();
            }

            return Ok(dailyCommutes);
        }

        // GET: api/Commutes/5
        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCommute([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dailyCommute = await _context.DailyCommutes.SingleOrDefaultAsync(m => m.id == id);

            if (dailyCommute == null)
            {
                return NotFound();
            }

            return Ok(dailyCommute);
        }

        // PUT: api/Commutes/5
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCommute([FromRoute] int id, [FromBody] DailyCommute dailyCommute)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dailyCommute.id)
            {
                return BadRequest();
            }

            _context.Entry(dailyCommute).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DailyCommuteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Commutes
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostCommute([FromBody] DailyCommute dailyCommute)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Check if this entry already exists, based on the date and not on the id
            if (DailyCommuteExists(dailyCommute.year, dailyCommute.month, dailyCommute.day))
            {
                return BadRequest(ModelState);
            }

            _context.DailyCommutes.Add(dailyCommute);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDailyCommute", new { id = dailyCommute.id }, dailyCommute);
        }

        // DELETE: api/Commutes/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCommute([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dailyCommute = await _context.DailyCommutes.SingleOrDefaultAsync(m => m.id == id);
            if (dailyCommute == null)
            {
                return NotFound();
            }

            _context.DailyCommutes.Remove(dailyCommute);
            await _context.SaveChangesAsync();

            return Ok(dailyCommute);
        }

        private bool DailyCommuteExists(int id)
        {
            return _context.DailyCommutes.Any(e => e.id == id);
        }

        private bool DailyCommuteExists(int year, int month, int day)
        {
            return _context.DailyCommutes.Any(e => (e.year == year) && (e.month == month) && (e.day == day) );
        }
    }
}